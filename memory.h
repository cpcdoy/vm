#ifndef MEMORY
#define MEMORY

#include "graphics.h"

#define PAGE__SIZE 0x1000
#define PAGE__MASK 0x0FFF

#define VIDEO_MEMORY__SIZE 0xA000 //Temporary

typedef struct VM_page
{

    struct VM_page* previous;
    struct VM_page* next;
    unsigned long base;
    unsigned int flag;
    unsigned int* actual_memory;

}VM_page;

typedef struct MEMORY
{

    unsigned int size;
    VM_page current_page;
    unsigned long base;
    unsigned int* actual_memory;

}VM_MEMORY;

typedef struct VIDEO_MEMORY
{

    unsigned int* pixels;

}VM_VIDEO_MEMORY;

#endif
