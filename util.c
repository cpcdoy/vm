#include <ctype.h>

#include "util.h"

bool isNumber(char *input)
{
    int i;
    for (i = 0; input[i] != '\0'; i++)
        if (isalpha(input[i]))
            return false;

    return true;
}
