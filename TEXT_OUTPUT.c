#include "TEXT_OUTPUT.h"

#define NaS ((string) {NULL, 0, 0})
#define isnas(S) (!(S)->s)

#define STR_FREEABLE (1ULL << 63)

#define STRINIT ((string) {malloc(16), 0, (16)})

#define stralloca(S) ((string) {alloca(S), 0, (S)})

#define stradupstr_aux(S)\
	__extension__ ({\
		char *_stradupstr_aux = alloca((S).size + 1);\
		memcpy(_stradupstr_aux, (S).s, (S).size);\
		strcstr_aux(_stradupstr_aux, (S).size);\
	})

#define stradupstr(S) strdupastr_aux(*(S))

#define S(C) stradupstr_aux(strcstr((char *)C))


#ifdef DEBUG_BOUNDS
#define S_C(S, I)\
	(* __extension__ ({\
		assert((I) >= 0);\
		assert((I) < (S)->size);\
		assert((I) < ((S)->b_size & ~STR_FREEABLE));\
		&((S)->s[I]);\
	}))
#else
#define S_C(S, I) ((S)->s[I])
#endif



 size_t strsize(string *s)
{
	if (isnas(s))
        return 0;

	return s->size;
}

 string strmalloc(size_t size)
{
	if (size < 16) size = 16;
	return (string) {malloc(size), 0, size | STR_FREEABLE};
}

 void strrealloc(string *s)
{
	char *buf;

	if (isnas(s))
        return;

	if (!(s->b_size & STR_FREEABLE))
        return;

	if (!s->size)
	{
		free(s->s);
		s->s = malloc(16);
	}
	else
	{
		buf = realloc(s->s, s->size);
		if (buf) s->s = buf;
	}
}

 void strresize(string *s, size_t size)
{
	char *buf;
	size_t bsize;

	if (isnas(s))
        return;

	if (!(s->b_size & STR_FREEABLE))
	{
		string s2;

		if (size <= s->size)
            return;

		s2 = strmalloc(size);

		memcpy(s2.s, s->s, s->size);

		s->s = s2.s;
		s->b_size = s2.b_size;

		return;
	}

	if (size & STR_FREEABLE)
	{
		free(s->s);
		*s = NaS;
		return;
	}

	bsize = s->b_size - STR_FREEABLE;

	if (size < 16)
        size = 16;

	if ((4 * size > 3 * bsize) && (size <= bsize))
        return;

	if ((size > bsize) && (size < bsize * 2))
        size = bsize * 2;

	if (size < 16)
        size = 16;

	buf = realloc(s->s, size);

	if (!buf)
	{
		free(s->s);
		*s = NaS;
	}
	else
	{
		s->s = buf;
		s->b_size = size | STR_FREEABLE;
	}
}

 void strfree(string *s)
{
	if (s->b_size & STR_FREEABLE)
        free(s->s);

	*s = NaS;
}



 string strcstr_aux(char *c, size_t len)
{
	return (string) {c, len, len + 1};
}

 string strcstr(char *c)
{
	size_t len = strlen(c);

	return strcstr_aux(c, len);
}

 string strdupstr(string *s)
{
	string s2;

	if (isnas(s))
        return NaS;

	s2 = strmalloc(s->size);
	s2.size = s->size;
	memcpy(s2.s, s->s, s->size);

	return s2;
}

 void strcpystr(string *dest, string *src)
{
	if (isnas(src))
        return;

	strresize(dest, src->size);

	if (isnas(dest))
        return;

	dest->size = src->size;
	memcpy(dest->s, src->s, src->size);
}

 char *strtocstr(string *s)
{
	size_t bsize;

	if (isnas(s))
        return NULL;

	bsize = s->b_size & ~STR_FREEABLE;

	if (s->size == bsize)
	{
		strresize(s, bsize + 1);

		if (isnas(s))
            return NULL;
	}

	s->s[s->size] = 0;

	return s->s;
}

 void strncatcstr(string *s, size_t len, const char *str)
{
	size_t bsize;

	if (isnas(s))
        return;

	if (!str || !len)
        return;

	bsize = s->b_size & ~STR_FREEABLE;

	if (s->size + len >= bsize)
	{
		strresize(s, s->size + len);

		if (isnas(s))
            return;
	}

	memcpy(&s->s[s->size], str, len);
	s->size += len;
}

 void strcatcstr(string *s, const char *str)
{
	if (str)
        strncatcstr(s, strlen(str), str);
}

 void strcatstr(string *s, const string *s2)
{
	strncatcstr(s, s2->size, s2->s);
}
