#ifndef HASH_TABLE
#define HASH_TABLE

typedef struct list_t
{

    char* str;
    struct list_t* next;

}VM_LIST;

typedef struct hash_table
{

    int size;
    VM_LIST** table;

}VM_HASH_TABLE;

//
VM_HASH_TABLE* CreateHashTable(int size);
unsigned int Hash(VM_HASH_TABLE* hashtable, char* str);
VM_LIST* LookUpString(VM_HASH_TABLE* hashtable, char* str);
int AddString(VM_HASH_TABLE* hashtable, char* str);
void FreeTable(VM_HASH_TABLE* hashtable);

#endif
