#include <stdio.h>
#include <stdlib.h>

#include "instructions.h"
#include "registers.h"
#include "CPU.h"
#include "memory.h"
#include "interrupts.h"
#include "VM.h"
#include "TEXT_OUTPUT.h"

bool running;
bool VGA_mode = false;

int fullOPCode, operand1, operand2, operand3, value;
int currentInstruction = HLT;
int tmp_registers[REGS_NUM];

unsigned int* pixels;

string vm_text_output;

VM_CPU* vm_cpu;

int* currentProgram;
int programSize;

void VM_Init(VM_CPU* cpu, int program[], int _programSize)
{
    vm_text_output = strmalloc(sizeof(char*)*30);
    printf("Initializing Virtual CPU...\n");

	running = true;
	operand1 = 0;
	operand2 = 0;
	operand3 = 0;
	value = 0;

	cpu->IP = 0;
	cpu->cores = CPU_coresCount();

	printf("Done.\n\n");

    if (GRAPHICS)
    {
        Init_Graphics();

        pixels = malloc(sizeof(unsigned int) * WINDOW_HEIGHT * WINDOW_WIDTH);
        memset(pixels, 0, sizeof(unsigned int) * WINDOW_HEIGHT * WINDOW_WIDTH);
    }
	programSize = _programSize;

    currentProgram = program;

	vm_cpu = cpu;


    #if (DEBUG)
        printf("\nSize of Program is : %d Instructions\n\n", programSize);
    #endif

    //output = (TEXT_OUTPUT*)malloc(sizeof(TEXT_OUTPUT));
    //strcpy(output->data, "test");
}

int Fetch()
{
        return currentProgram[vm_cpu->IP++ < programSize ? vm_cpu->IP : -1];
}

void Decode(int instruction)
{
    fullOPCode = instruction;
	currentInstruction = (instruction & 0xF000) >> 12;
	operand1     	   = (instruction & 0xF00 ) >>  8;
	operand2    	   = (instruction & 0xF0  ) >>  4;
	operand3    	   = (instruction & 0xF   );
	value     	       = (instruction & 0xFF  );
}

void EvaluateDecodedInstruction()
{
    switch(currentInstruction)
    {
        case HLT:
            #if (DEBUG)
                printf("%d : ",vm_cpu->IP);
                printf("Halting...\n");
                strcatcstr(&vm_text_output, "Halting...\n");
            #endif
        running = false;
        break;

        case MOV:
            #if (DEBUG)
                printf("%d : ",vm_cpu->IP);
                printf("MOV R%d %d\n", operand1, value);
                strcatcstr(&vm_text_output, "MOV ");
                char buf[6];
                snprintf(buf, 5, "%d",operand1);
                strcatcstr(&vm_text_output, buf);
                strcatcstr(&vm_text_output, " ");
                snprintf(buf, 5, "%d",operand2);
                strcatcstr(&vm_text_output, buf);
                strcatcstr(&vm_text_output, "\n");
            #endif
            vm_cpu->registers[operand1] = value;
        break;

        case ADD:
            #if (DEBUG)
                printf("%d : ",vm_cpu->IP);
                printf("ADD R%d R%d\n", operand1, operand2);
                strcatcstr(&vm_text_output, "Halting...\n");
            #endif
            vm_cpu->registers[operand1] = vm_cpu->registers[operand1] + vm_cpu->registers[operand2];
        break;

        case SUB:

            #if (DEBUG)
                printf("%d : ",vm_cpu->IP);
                printf("SUB R%d R%d\n", operand1, operand2);
            #endif
            vm_cpu->registers[operand1] = vm_cpu->registers[operand1] - vm_cpu->registers[operand2];
        break;

        case MUL:

            #if (DEBUG)
                printf("%d : ",vm_cpu->IP);
                printf("MUL R%d R%d\n", operand1, operand2);
            #endif
            vm_cpu->registers[operand1] = vm_cpu->registers[operand1] * vm_cpu->registers[operand2];
        break;

        case INT:
        #if (DEBUG)
            printf("INT ");
        #endif
            switch(value)
            {
                case VIDEO__INT:
                     switch (vm_cpu->registers[0])
                     {
                        case VIDEO__SET_MODE && vm_cpu->registers[0] == VIDEO__VGA_MODE:

                            #if (DEBUG)
                                printf("0x10");
                            #endif
                            VGA_mode = true;
                            VIDEO_Interpret_SET_MODE();
                        break;

                        case VIDEO__WRITE_PIXEL:
                        if (VGA_mode)
                        {
                            #if (DEBUG)
                                printf("0x10");
                            #endif
                            VM_PIXEL pixel = {{vm_cpu->registers[1], vm_cpu->registers[2], vm_cpu->registers[3]},vm_cpu->registers[4], vm_cpu->registers[5]};
                            printf("\npixel : %d, %d, %d", (int)pixel.color.r, (int)pixel.color.g, (int)pixel.color.b);
                            printf("\nPosition : %d, %d", pixel.X, pixel.Y);
                            VIDEO_Interpret_WRITE_PIXEL(pixel);
                        }

                        break;
                     }
                break;

                case LLDS__INT:
                    if (vm_cpu->registers[0] == LLDS__RESET_DISK)
                    {
                        #if (DEBUG)
                            printf("0x13\n");
                        #endif
                        //LLDS_Interpret_RESET_DISK();
                    }
                break;
            }

        break;

        case THREAD:

        //TODO

        break;
    }

    switch(fullOPCode)
    {
        //case
    }
}

void Run()
{
	while(running == true)
    {
        #if (DEBUG)
            int i;

            for (i = 0; i < REGS_NUM; i++)
                printf("R%d    ", i);
            printf("\n");
            for (i = 0; i < REGS_NUM; i++)
                printf("%04x  ", vm_cpu->registers[i]);
            printf("\n");
        #endif

		Decode(Fetch());
		EvaluateDecodedInstruction();
		printf("\n");
	}

	while (VGA_mode)
        Update(vm_text_output.s, pixels);
}

void SetPixel(VM_PIXEL pixel)
{
    pixels[pixel.Y * WINDOW_WIDTH + pixel.X] = (0xFF << 24) | (pixel.color.r << 16) | (pixel.color.g << 8) | (pixel.color.b);
}

void VM_Exit()
{
    strfree(&vm_text_output);
    free(pixels);
}
