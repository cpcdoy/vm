#include <stdlib.h>
#include <string.h>

#include "Hash_table.h"

VM_HASH_TABLE* CreateHashTable(int size)
{
    VM_HASH_TABLE *new_table;

    if (size < 1)
        return NULL;

    if ((new_table = malloc(sizeof(VM_HASH_TABLE))) == NULL)
        return NULL;

    if ((new_table->table = malloc(sizeof(VM_LIST *) * size)) == NULL)
        return NULL;

    int i;
    for(i = 0; i < size; i++)
        new_table->table[i] = NULL;

    new_table->size = size;

    return new_table;
}

unsigned int Hash(VM_HASH_TABLE *hashtable, char *str)
{
    unsigned int hashval = 0;

    for(; *str != '\0'; str++)
        hashval = *str + (hashval << 5) - hashval;

    return hashval % hashtable->size;
}

VM_LIST *LookUpString(VM_HASH_TABLE *hashtable, char *str)
{
    VM_LIST *list;
    unsigned int hashval = Hash(hashtable, str);

    for(list = hashtable->table[hashval]; list != NULL; list = list->next)
        if (strcmp(str, list->str) == 0)
            return list;

    return NULL;
}

int AddString(VM_HASH_TABLE *hashtable, char *str)
{
    VM_LIST *new_list;
    VM_LIST *current_list;
    unsigned int hashval = Hash(hashtable, str);

    if ((new_list = malloc(sizeof(VM_LIST))) == NULL)
        return 1;

    current_list = LookUpString(hashtable, str);

    if (current_list != NULL)
        return 2;

    new_list->str = strdup(str);
    new_list->next = hashtable->table[hashval];
    hashtable->table[hashval] = new_list;

    return 0;
}

void FreeTable(VM_HASH_TABLE *hashtable)
{
    int i;
    VM_LIST *list, *temp;

    if (hashtable == NULL)
        return;

    for(i = 0; i < hashtable->size; i++)
    {
        list = hashtable->table[i];
        while(list!=NULL)
        {
            temp = list;
            list = list->next;
            free(temp->str);
            free(temp);
        }
    }

    free(hashtable->table);
    free(hashtable);
}
