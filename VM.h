#ifndef VM
#define VM

#include "util.h"
#include "graphics.h"

#define DEBUG true
#define GRAPHICS true

typedef struct HEADER
{

	unsigned int version;

	unsigned int codeOffset;
	unsigned int codeSize;

	unsigned int dataOffset;
	unsigned int dataSize;

	unsigned int programOffset;
	unsigned int programSize;

	unsigned int fileSize;

}VM_HEADER;

//Funcs
void VM_Init();
void EvaluateDecodedInstruction();
void Run();
void SetPixel(VM_PIXEL pixel);
void VM_Exit();


//Main
void RetrieveProgramFromFile(char* filename, int* program, int programSize);
int CountLines(char *filename);

#endif
