#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "CPU.h"
#include "VM.h"
#include "graphics.h"
#include "Hash_table.h"
#include "instructions.h"

void Exit(void) __attribute__((destructor));
VM_CPU* cpu;
VM_HASH_TABLE* label_table;

int main(int argc, char *argv[])
{
    cpu = (VM_CPU*)malloc(sizeof(VM_CPU));

    label_table = CreateHashTable(10);

    int programSize;
    if (strcmp("-f", argv[1]) == 0)
        programSize = CountLines(argv[2])-1;
    else
    {
        printf("Help : \n Args : -f \"filename\"\n");
        exit(EXIT_FAILURE);
    }

    int program[programSize];

    RetrieveProgramFromFile(argv[2], program, programSize);

	VM_Init(cpu, program, programSize);
	Run();

	return 0;
}

void RetrieveProgramFromFile(char* filename, int* program, int programSize)
{
    FILE *fp = fopen(filename,"r");
    int c = 0, i = 0, j = 0;
    char currentLine[11];

    if (fp == NULL)
        return;

    while ((c = fgetc(fp)) != EOF)
    {
        if (c == '\n')
        {
            i = 0;
            if (isNumber(&(currentLine)+2))
                program[j] = (int)strtol(currentLine, (char**)NULL, 0);
            else
            {
                AddString(label_table, currentLine);
                program[j] = 0xFFFF;
            }
            j++;
        }
        currentLine[i] = c;
        i++;
    }
    fclose(fp);
}

int CountLines(char *filename)
{
    FILE *fp = fopen(filename,"r");
    int ch = 0;
    int lines=0;

    if (fp == NULL)
        return 0;

    lines++;
    while ((ch = fgetc(fp)) != EOF)
    {
        if (ch == '\n')
        lines++;
    }
    fclose(fp);
    return lines;
}

void Exit(void)
{
    VM_Exit();
    //free(cpu);
    #if GRAPHICS
        printf("Closing GRAPHIC CONTEXT");
        //Exit_Graphics();
    #endif // GRAPHICS
    printf("\nExiting...\n");
}
