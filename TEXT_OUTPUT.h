#ifndef TEXT_OUTPUT
#define TEXT_OUTPUT

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct string string;

struct string
{
	char *s;
	size_t size;
	size_t b_size;

};

//
 size_t strsize(string* s);
 string strmalloc(size_t size);
 void strrealloc(string *s);
 void strresize(string *s, size_t size);
 void strfree(string *s);
 string strcstr_aux(char *c, size_t len);
 string strcstr(char *c);
 string strdupstr(string *s);
 void strcpystr(string *dest, string *src);
 char *strtocstr(string *s);
 void strncatcstr(string *s, size_t len, const char *str);
 void strcatcstr(string *s, const char *str);
 void strcatstr(string *s, const string *s2);

#endif
