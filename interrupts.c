#include "interrupts.h"
#include "VM.h"

#include <stdio.h>

void VIDEO_Interpret_SET_MODE()
{
    printf("\n\nVIDEO MODE : VGA (640*480)");

    /*asm("mov %ah, 0x0\n"
        "mov %al, 0x12\n"
        "int $0x10");*/
}

void VIDEO_Interpret_WRITE_PIXEL(VM_PIXEL pixel)
{
    SetPixel(pixel);
}

void LLDS_Interpret_RESET_DISK()
{
    //NOT SAFE !
    asm("mov %ax, 0x0\n"
        "int $0x13");
}
