#ifndef GRAPHICS_
#define GRAPHICS_

#include "util.h"
#include <SDL2/SDL.h>

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

#define WINDOW_TITLE "VM_GRAPHICS"

typedef struct PIXEL
{
    SDL_Color color;
    int X, Y;

}VM_PIXEL;

//
void Init_Graphics();
void Update();
int GetScreenPitch();
SDL_Surface* GetScreen();
void Exit_Graphics();

#endif
