#ifndef INTERRUPTS
#define INTERRUPTS

#include "graphics.h"
//RESULT IS STORED IN R0
//FORMAT : INT XX -> 0xE0XX

//LOAD_OS
#define LOAD_OS__INT 0x19
//
//LOW LEVEL DISK SERVICES
#define LLDS__INT 0x13

#define LLDS__RESET_DISK 0x0

void LLDS_Interpret_RESET_DISK();
//
//VIDEO
#define VIDEO__INT 0x10

#define VIDEO__SET_MODE 0x0 // R0 = 0x12 (VIDEO__VGA_MODE) -> 640x480 color graphics (VGA)
#define VIDEO__VGA_MODE 0x12
#define VIDEO__SET_CURSOR_POSITION 0x2
#define VIDEO__GET_CURSOR_POSITION 0x3 //Also, gets shape of cursor
#define VIDEO__SET_DISPLAY_PAGE 0x5
#define VIDEO__WRITE_CHAR_AT_CURSOR 0xA
#define VIDEO__WRITE_PIXEL 0xC // R0 = 0xC, R1/R2/R3 = R,G,B, R4/R5 = X/Y, R6 = display page (optional)
#define VIDEO__READ_PIXEL 0xD
#define VIDEO__WRITE_CHAR_TTY_MODE 0xE //Char to write is to be put in R1
#define VIDEO__GET_MODE 0xF

void VIDEO_Interpret_SET_MODE();

void VIDEO_Interpret_WRITE_PIXEL(VM_PIXEL pixel);
//
//KEYBOARD
#define KEYBOARD__INT 0x16

#define KEYBOARD__READ_CHAR 0x0
#define KEYBOARD__STORE_KEY_IN_BUFFER 0x5
//
//REAL TIME CLOCK (RTC)
#define RTC__INT 0x1A

#define RTC__READ 0x0
#define RTC__SET 0x1
#define RTC__READ_TIME 0x2
#define RTC__SET_TIME 0x3
#define RTC__READ_DATE 0x4
#define RTC__SET_DATE 0x5
//

#endif
