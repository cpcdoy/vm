#ifndef CPU
#define CPU

#include "registers.h"

typedef struct CPU
{

	int IP;

	int registers[REGS_NUM];

	int cores;

}VM_CPU;


//Func
int Fetch();
void Decode(int instruction);
int CPU_coresCount();

#endif
