#ifndef INSTRUCTIONS
#define INSTRUCTIONS

//Format : 0xX000
#define HLT 0x0
#define MOV 0x1
#define ADD 0x2
#define SUB 0x3
#define MUL 0x4
#define JMP 0x5
#define LBL 0x6
#define INT 0xE
#define THREAD 0xF

//Format : 0x000X

#endif
