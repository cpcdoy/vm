#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

/*#include <SFML/Graphics.h>
#include <SFML/Window.h>*/

#include "graphics.h"

SDL_Window* window = NULL;

SDL_Surface* surface = NULL;

SDL_Renderer * renderer = NULL;
SDL_Texture * textTexture;
SDL_Texture * pixelsTexture;

SDL_Event event;


TTF_Font* font = NULL;

int iW, iH, ix, iy;
int textLines;

bool GRAPHICS_Context = true;
char* emptyText = "";

void renderTexture2(SDL_Texture *tex, SDL_Renderer *ren, SDL_Rect dst, SDL_Rect *clip)
{
    SDL_RenderCopy(ren, tex, clip, &dst);
}

void RenderTexture(SDL_Texture* tex, SDL_Renderer* rend, int x, int y, SDL_Rect* clip)
{
        SDL_Rect dst;
        dst.x = x;
        dst.y = y;
        if (clip != NULL)
        {
            dst.w = clip->w;
            dst.h = clip->h;
        }
        else
            SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);

        renderTexture2(tex, rend, dst, clip);
}

SDL_Texture* renderTextOnTexture(char* text, SDL_Color color)
{
    surface = TTF_RenderText_Blended_Wrapped(font, text, color, WINDOW_WIDTH);

    return SDL_CreateTextureFromSurface(renderer, surface);
}

void RenderTextOnScreen(char* text, int X, int Y, SDL_Color text_color, int textLineOffset)
{

    textTexture = renderTextOnTexture(text, text_color);
    RenderTexture(textTexture, renderer, X, Y+=textLineOffset, NULL);
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(textTexture);
}

void Init_Graphics()
{
     printf("Loading GRAPHIC CONTEXT...\n");
     if( SDL_Init( SDL_INIT_VIDEO ) != 0)
       printf( "Could not initialize GRAPHICS! SDL2_Error: %s\n", SDL_GetError() );
     else
     {
        if (TTF_Init() != 0)
            printf("Could not initialize SDL_ttf");

        window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

        pixelsTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, WINDOW_WIDTH, WINDOW_HEIGHT);

        font = TTF_OpenFont("terminus.ttf", 12);
        if (font == NULL)
            printf("Font not found !");


        SDL_QueryTexture(textTexture, NULL, NULL, &iW, &iH);

        ix = 0;
        iy = 0;

        printf("Done.\n");
     }
}

int GetScreenPitch()
{
    return surface->pitch;
}

SDL_Surface* GetScreen()
{
    return surface;
}



void Draw()
{

}

void Update(char* text, unsigned int* pixels)
{
    while( SDL_PollEvent( &event ) != 0 )
       if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
            exit(0);

    SDL_RenderClear(renderer);

    SDL_UpdateTexture(pixelsTexture, NULL, pixels, WINDOW_WIDTH * sizeof(unsigned int));

    SDL_RenderCopy(renderer, pixelsTexture, NULL, NULL);

    SDL_Color text_color = {0, 255, 0, 255};

    RenderTextOnScreen(text, ix, iy, text_color, 6);

    SDL_RenderPresent(renderer);
}


void Exit_Graphics()
{
     SDL_DestroyTexture(pixelsTexture);
     SDL_DestroyTexture(textTexture);
     SDL_DestroyRenderer(renderer);
     SDL_DestroyWindow( window );
     SDL_FreeSurface(surface);
     TTF_CloseFont(font);
     TTF_Quit();
     SDL_Quit();
}

/* SFML Test
sfContextSettings* windowSettings = {24, 8, 0};
sfVideoMode videoMode = {800, 600, 32};
sfRenderWindow* mainWindow;
sfImage* image;
sfSprite* sprite;
sfFont* font;
sfText* text;
sfEvent event;

void Init_Graphics()
{
    mainWindow = sfRenderWindow_create(videoMode, "VM_GRAPHICS", sfClose, windowSettings);
    font = sfFont_createFromFile("terminus.ttf");

    text = sfText_create();
    sfText_setString(text, "Test");
    sfText_setFont(text, font);

    sfVector2f v = {32, 32};
    sfText_scale(text, v);
}

void Update()
{
        sfEvent* event;

        sfContext* context = sfContext_create();
        sfContext_setActive(context, sfTrue);

        while (sfRenderWindow_isOpen(mainWindow) == sfTrue)
        {
            sfRenderWindow_clear(mainWindow, sfBlack);

            sfRenderWindow_drawText(mainWindow, text, 0);

            sfRenderWindow_display(mainWindow);
        }

    sfContext_destroy(context);
}*/
//SFML Test
